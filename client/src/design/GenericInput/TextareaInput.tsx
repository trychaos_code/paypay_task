import React, {Component, ReactElement} from 'react';
import cns from 'classnames';

interface Props {
    name: string,
    value?: string,
    isBig?: boolean,
    parentClassName?:string,
    label?:string | ReactElement,
    outputValueModifier?:Function,
    inputValueModifier?:Function,
    onChange?:Function
}
export class TextareaInput extends Component<Props> {
    render() {
        const { isBig, parentClassName, label, outputValueModifier, inputValueModifier, ...passProps } = this.props,
            parentClass = cns('form-group', parentClassName),
            cols = isBig ? 40 : 20;

        return (
            <div className={parentClass}>
                <label>
                    {label ? <span className="form-label">{label}</span> : null}
                    <div className="form-field">
                        <textarea className="input-group" rows={3} cols={cols} {...passProps} onChange={this.onChange} />
                    </div>
                </label>
            </div>
        );
    }

    onChange = (e: { target: { value: any; }; }) => {
        const {name, onChange} = this.props;
        onChange && onChange(name, e.target.value);
    };
}
