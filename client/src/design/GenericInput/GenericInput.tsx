import React, { Fragment, PureComponent } from 'react';
import cns from 'classnames';

const supportedTypes = {
    text: 'text',
    date: 'date',
    'datetime-local': 'datetime-local',
    email: 'email',
    month: 'month',
    number: 'number',
    search: 'search',
    time: 'time',
    week: 'week',
    password: 'password',
};

const COMMIT_TIMEOUT = 500;

interface Props {
    name: string,
    value: any,
    onChange: Function,
    focus?: boolean,
    disabled?: boolean,
    min?: number,
    max?: number,
    type?:string,
    label?:string,
    inputClassNames?:string,
    inputValueModifier?:Function,
    outputValueModifier?:Function,
    className?:string,
}
export class GenericInput extends PureComponent<Props> {
    __pendingCommit: boolean;
    _focusTimer: any;
    __commitTimer: any;
    textInput: any;
    state = {
        value: this.props.value,
        message: '',
        isFocused: false,
        showPassword: false
    };

    // @ts-ignore
    componentWillReceiveProps({ value }) {
        // Prevents any new prop changes from changing whats currently in the field in case
        // the user is active on it and has a pending change.
        if (!this.__pendingCommit) {
            this.setState({ value });
        }
    }

    componentWillUnmount() {
        if (this._focusTimer) {
            this.__pendingCommit = false;
            clearTimeout(this._focusTimer);
        }
    }

    componentDidMount() {
        if (this.props.focus) {
            //Timeout added to override the react select focus after 50ms.
            this._focusTimer = setTimeout(() => {
                this.textInput.focus();
            }, 51);
        }
    }

    render() {
        const {
            type,
            label,
            inputClassNames,
            inputValueModifier,
            outputValueModifier,
            className
        } = this.props;

        const {showPassword, value} = this.state;

        const classed = cns('form-group', className);
        const passProps:any = {};
        passProps.value = value !== undefined ? value : '';
        if (inputValueModifier) {
            passProps.value = inputValueModifier(passProps.value);
        }

        if (type === 'number' && isNaN(passProps.value)) {
            passProps.value = '';
        }

        if (passProps.value === undefined || passProps.value === null) {
            passProps.value = '';
        }

        const disabled = this.props.disabled || this.context.disabledInputs;
        let inputProps = {
            type: showPassword || !type ? supportedTypes.text : type,
            className: cns(
                'form-control',
                inputClassNames
            ),
            ref: (input: any) => (this.textInput = input),
            disabled,
            onChange: this.onChange,
            onFocus: this.onFocus,
            onBlur: this.onBlur,
            ...passProps,
        };

        if (inputProps.type === supportedTypes.number) {
            if (
                (!inputProps.min && inputProps.min !== 0) ||
                inputProps.min < Number.MIN_SAFE_INTEGER
            ) {
                inputProps.min = Number.MIN_SAFE_INTEGER;
            }
            if (
                (!inputProps.max && inputProps.max !== 0) ||
                inputProps.max > Number.MAX_SAFE_INTEGER
            ) {
                inputProps.max = Number.MAX_SAFE_INTEGER;
            }
        }

        const input = (
            <div className="password-input input-group form-field">
                <input {...inputProps} />
                {type === supportedTypes.password ? (
                    <button
                        type="button"
                        className="password-input-show-icon"
                        onClick={this.toggleShowPassword}
                    >
                        {showPassword ? 'Hide' :'Show'}
                    </button>
                ) : null}
            </div>
        );

        if (label) {
            return (
                <div className={classed}>
                    <label>
                        {label}
                        {input}
                    </label>
                </div>
            );
        } else {
            return (
                <div className={classed}>
                    {input}
                </div>
            );
        }
    }
    toggleShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    };

    validateAndRestrictValue = (value: number) => {
        const { type, min, max } = this.props;
        if (type === supportedTypes.number) {
            const minVal =
                (!min && min !== 0) || min < Number.MIN_SAFE_INTEGER
                    ? Number.MIN_SAFE_INTEGER
                    : min;
            const maxVal =
                (!max && max !== 0) || max > Number.MAX_SAFE_INTEGER
                    ? Number.MAX_SAFE_INTEGER
                    : max;
            if (value < minVal) {
                value = minVal;
            } else if (value > maxVal) {
                value = maxVal;
            }
        }
        return value;
    };

    onChange = (e: { target: { value: number; }; }) => {
        let value = e.target.value;

        const validatedValue = this.validateAndRestrictValue(value);
        if (validatedValue !== value) {
            e.target.value = validatedValue;
            value = validatedValue;
        }

        this.setState({
            value: this.props.outputValueModifier ? this.props.outputValueModifier(value) : value,
        });
        clearTimeout(this.__commitTimer);
        this.__pendingCommit = true;
        this.__commitTimer = setTimeout(() => this.onCommit(), COMMIT_TIMEOUT);
    };

    onCommit = () => {
        const { value } = this.state;
        this.__pendingCommit = false;
        clearTimeout(this.__commitTimer);
        if (value !== this.props.value) {
            this.props.onChange(this.props.name, value);
        }
    };


    onBlur = () => {
        this.setState({ isFocused: false });
        this.onCommit();
    };

    onFocus = () => {
        this.setState({ isFocused: true });
    };

}
