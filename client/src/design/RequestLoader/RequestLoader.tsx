import React, {Component, Fragment, Children, cloneElement, isValidElement} from 'react';
import cns from 'classnames';
import {Request, Method, i18n} from '../../utils';

export interface Props {
    className?: string,
    method?: Method,
    url: string,
    loadingText?: string,
    param?: object
    onReqComplete?: Function
}
export class RequestLoader extends Component<Props> {
    _isMounted = false;
    state: {
        isLoading: boolean,
        fetchedData: null | Response,
        err: null | Error
    } = {
        isLoading: true,
        fetchedData: null,
        err: null
    };
    render() {
        const {
            loadingText = i18n('Loading'),
            className,
            children
        } = this.props;
        const {
            isLoading, fetchedData, err
        } = this.state;
        return (
          <Fragment>
              {
                  isLoading ? (
                    <div className={cns('loading', className)}>
                        {`${loadingText} ...`}
                    </div>
                  ) : (
                    err && err.message ? (
                      <div>Error with message: {err.message}</div>
                    ) : (
                      Children.map(children, child =>
                        isValidElement(child) ? cloneElement(child, {data: fetchedData} || {}) : null
                      )
                    )
                  )
              }
          </Fragment>
        );
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    fetchData = () => {
        const {
            method = 'GET',
            url,
            param,
            onReqComplete
        } = this.props;
        const req = new Request(url, method, param);
        req.then((res, err) => {
            onReqComplete && onReqComplete(res);
            this._isMounted && this.setState({
                fetchedData: res,
                err: err,
                isLoading: false
            })
        });
    };
    componentDidMount(): void {
        this._isMounted = true;
        this.fetchData();
    }
    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<{}>, snapshot?: any): void {
        if(prevProps.url !== this.props.url) {
            this.fetchData();
        }
    }
}
