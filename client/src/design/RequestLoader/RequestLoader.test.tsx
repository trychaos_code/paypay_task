import { cleanup, render } from '@testing-library/react';
import { RequestLoader, Props } from './RequestLoader';
import React, {PureComponent} from 'react';
import { Method } from '../../utils';

const defaultProps = {
  className: 'string',
  url: 'url',
  loadingText: 'loadingText'
};
describe('<RequestLoader />', () => {
  function renderRequestLoader(props: Partial<Props> = {}) {
    // @ts-ignore
    const renderedElm = render(<RequestLoader {...props} />);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderRequestLoader(defaultProps);
  });
});
