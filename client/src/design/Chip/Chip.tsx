import React, {Fragment, PureComponent, ReactElement, isValidElement, cloneElement, Children} from 'react';
import cns from "classnames";
import {Img} from "../Img"

export interface Props {
    title?: string | number | ReactElement,
    body?: string | number | ReactElement,
    icon?: string,
    iconText?: string,
    className?: string
    replaceBody?: boolean
}

export class Chip extends PureComponent<Props> {
    render() {
        const {
            title = '', body = '', className, icon, iconText, replaceBody
        } = this.props;
        return (
            <div className={cns("chip", className)}>
                {
                    icon ? <div className="avatar">
                        <Img src={icon} alt="avatar"/>
                    </div> : (
                        <div className="avatar">
                            <span>{
                                iconText ? iconText : (title ? title.toString()[0] : body.toString()[0])
                            }</span>
                        </div>
                    )
                }
                {
                    title ? <Fragment>
                        <div className="chip-title">{title}</div>
                        <div className="chip-separator"> |</div>
                    </Fragment> : null
                }
                {
                    replaceBody ? Children.map(body, child =>
                        isValidElement(child) ? cloneElement(child, {
                            className: cns('chip-body', child.props.classNames)
                        }) : null) : <div className="chip-body">{body}</div>
                }
            </div>
        );
    }
}
