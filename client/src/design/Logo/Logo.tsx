import React, {ReactElement, PureComponent} from 'react';
import cns from 'classnames';
import {Img} from '../Img';

export interface Props {
    img: string,
    className?: string,
    style?: object
}
export class Logo extends PureComponent<Props> {
    render() {
        const {
            img,
            className,
            style = {}
        } = this.props;
        return (
          <Img src={img} className={cns('logo', className)} style={style}/>
        );
    }
}
