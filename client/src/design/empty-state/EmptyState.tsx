import React, {ReactElement, PureComponent} from 'react';

export interface Props {
    emptyIcon: string | ReactElement,
    title: string | ReactElement,
    subtitle: string | ReactElement,
    action: string | ReactElement
}
export class EmptyState extends PureComponent<Props> {
    render() {
        const {
            emptyIcon, title, subtitle, action
        } = this.props;
        return (
          <div className="empty">
              <div className="empty-icon">
                  {emptyIcon}
              </div>
              <p className="empty-title">
                  {title}
              </p>
              <p className="empty-subtitle">
                  {subtitle}
              </p>
              <div className="empty-action">
                  {action}
              </div>
          </div>
        );
    }
}
