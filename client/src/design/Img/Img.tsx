import React, {ReactElement, PureComponent} from 'react';
import {IconList} from '../../cached-components';

export interface Props {
    src: string
    className?: string,
    style?: object
    alt?: string | undefined | null
}
export class Img extends PureComponent<Props> {
    render() {
        const {src, ...rest} = this.props;
        // @ts-ignore
        return  <img src={IconList[src] || src} {...rest} />;
    }
}
