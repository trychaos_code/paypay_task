import { cleanup, render } from '@testing-library/react';
import { Img, Props } from './Img';
import React, {PureComponent} from 'react';

const defaultProps = {
  src: 'src',
  alt: 'alt'
};
describe('<Img />', () => {
  function renderImg(props: Partial<Props> = {}) {
    props = {...defaultProps, ...props};
    // @ts-ignore
    const renderedElm = render(<Img {...props} />);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderImg(defaultProps);
  });
});
