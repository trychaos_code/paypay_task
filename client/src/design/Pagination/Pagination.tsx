import React, {PureComponent} from 'react';
import {Button} from '../Button';
import {i18n} from "../../utils";

export interface Props {
  totalPage: number
  pageNumber?: number
  itemPerPage?: number
  onPageChange?: Function
}
export class Pagination extends PureComponent<Props> {
  _isMounted = false;
  state = {
    pageNumber: this.props.pageNumber || 1
  };
  render() {
    const { totalPage=1 } = this.props;
    const { pageNumber=1 } = this.state;
    return (
      <div className="pagination">
        <div className="pagination-container">
          <Button isText={true} disabled={pageNumber === 1} onClick={() => this.onPageChange(-1 * totalPage)}>{i18n('First')}</Button>
          <Button isText={true} disabled={pageNumber === 1} onClick={() => this.onPageChange(-1)}>{i18n('Previous')}</Button>
          <div className="btn">
            {i18n('Page')} {pageNumber} {i18n('of')} {totalPage}
          </div>
          <Button isText={true} disabled={pageNumber === totalPage} onClick={() => this.onPageChange(1)}>{i18n('Next')}</Button>
          <Button isText={true} disabled={pageNumber === totalPage} onClick={() => this.onPageChange(totalPage)}>{i18n('Last')}</Button>
        </div>
      </div>
    );
  }
  componentDidMount() {
    this._isMounted = true;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  onPageChange = (noOfPage: number) => {
    const { totalPage, onPageChange } = this.props;
    const { pageNumber } = this.state;
    let newPage = (pageNumber + noOfPage) || 1;
    if(newPage > totalPage) {
      newPage = totalPage;
    } else if (newPage < 1) {
      newPage = 1;
    }
    this._isMounted && this.setState({pageNumber: newPage}, () => {
      onPageChange && onPageChange(newPage);
    });
  }
}
