import React, {Fragment, Children, cloneElement, Component, isValidElement, ReactElement} from "react";
import cns from "classnames";
interface Props {
    message: string | ReactElement
    position: 'top' | 'bottom' | 'left' | 'right'
}

export class Tooltip extends Component<Props> {
    state = {
        displayTooltip: false
    };

    hideTooltip = () => {
        this.setState({displayTooltip: false})
    };

    showTooltip = () => {
        this.setState({displayTooltip: true})
    };

    render() {
        let {message, position, children} = this.props;
        let {displayTooltip} = this.state;
        return (<Fragment>
            <div className='tooltip' onMouseLeave={this.hideTooltip}>
                {
                    displayTooltip ? (<div className={`tooltip-bubble tooltip-${position}`}>
                        <div className='tooltip-message'>{message}</div>
                    </div>) : null
                }
                {
                    Children.map(children, child =>
                        isValidElement(child) ? cloneElement(child, {
                            className: cns('tooltip-trigger', child.props.className),
                            onMouseOver: this.showTooltip
                        }) : null)
                }
            </div>
        </Fragment>)
    }
}
