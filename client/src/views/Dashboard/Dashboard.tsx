import React, {Component} from 'react';
import {List, NavFilter} from '../../hoc';
import {RequestLoader} from '../../design';
import {IEmpResponse, Ifilters} from '../../types';
import {getUserDet, i18n} from "../../utils";

interface Props {
}
const DEFAULT_EMP_URL = "/api/employee?page=1";
export class Dashboard extends Component<Props> {
    _isMounted = false;
    userDet = getUserDet();
    state: { employeeUrl: string, filters: Ifilters, page: number, selectedFilters: any } = {
        employeeUrl: DEFAULT_EMP_URL,
        filters: {},
        selectedFilters: {},
        page: 1
    };

    render() {
        const {employeeUrl, filters} = this.state;
        return (<div className="page-container">
            <NavFilter onFilterChange={this.onFilterChange} filters={filters}/>
            <RequestLoader url={employeeUrl} loadingText={i18n("Fetching details")} onReqComplete={this.onReqComplete}>
                <List listTitle={`${i18n('Hi')} ${this.userDet.NAME} !! ${i18n('Available employee')}`} onPageChange={this.onPageChange}/>
            </RequestLoader>
        </div>);
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    componentDidMount(): void {
        this._isMounted = true;
    }

    onReloadList = () => {
        const {selectedFilters, page} = this.state;
        // @ts-ignore
        const uriParams = Object.entries(selectedFilters).map(([k,i]:{uuid: string})=>{
            let i1 = i.uuid || i;
            return {key: k, value: i1 && i1.toString() !== "-1" ? i1 : ''}
        }).filter((item: any)=>item.value);
        let employeeUrl = DEFAULT_EMP_URL.split('?')[0]+ "?" + 'page='+ page + '&'
            +uriParams.map(({key, value}: { key: string; value: string; })=>(`${key}=${value}`)).join('&');
        employeeUrl.endsWith('&') && (employeeUrl = employeeUrl.substring(0, employeeUrl.length-1))
        this._isMounted && this.setState({
            employeeUrl
        })
    };
    onFilterChange = (selectedFilters: Ifilters) => {
        this._isMounted && this.setState({
            selectedFilters
        }, this.onReloadList)
    };
    onPageChange = (page:number) => {
        this._isMounted && this.setState({
            page
        }, this.onReloadList)
    };
    onReqComplete = ({filters: {BU_NO, GRADE, JOB, MANAGER, NAME, TYPE, AUTH_TYPE}}: IEmpResponse) => {
        this._isMounted && this.setState({
            filters: {
                BU_NO, GRADE, JOB, MANAGER, NAME, TYPE, AUTH_TYPE
            }
        })
    }
}
