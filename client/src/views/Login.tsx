import React, {Component} from 'react';
import {Button} from '../design/Button';
import {GenericInput} from "../design/GenericInput";
import {signInFields} from "../types";
import {Request, makeLogin, getToken, i18n} from "../utils";


interface Props {

}

export class Login extends Component<Props> {
    state: { formFields?: any } = {formFields: JSON.parse(JSON.stringify(signInFields))};

    getFormItems = ([k, {type, value}]: any, index: string | number | undefined) => {
        return <GenericInput key={index} label={k} type={type} name={k} value={value}
                             onChange={this.onChange}/>
    };

    render() {
        const {formFields} = this.state;
        return (
            <div className="form-horizontal">
                <h1>{i18n('Sign In')}</h1>

                {
                    formFields ? Object.entries(formFields).map(this.getFormItems) : null
                }

                <Button disabled={!!Object.values(formFields).find(({value}) => !value)} fullWidth={true}
                        onClick={this.onSubmitChange}> {i18n('Login')} </Button>
            </div>
        )
    }
    componentDidMount(): void {
        if(getToken()) {
            location.href = location.origin
        }
    }

    onChange = (name: string, value: string) => {
        const {formFields} = this.state;
        formFields[name].value = value;
        this.setState({formFields});
    };
    onSubmitChange = () => {
        const {formFields} = this.state;
        console.log('formFields', this.state.formFields);
        (async () => {
            const res = await new Request('/api/login', 'POST', Object.entries(formFields).reduce((acc, [k, v]) => {
                // @ts-ignore
                acc[k] = v.value;
                return acc;
            }, {}));
            res && makeLogin(res)
        })();
    }
}

export default Login
