import {Dashboard} from './Dashboard';
import {NotFound} from './NotFound';
import {AddEmployee} from './AddEmployee';
import {Login} from './Login';
import {Logout} from './Logout';

export {Dashboard, NotFound, AddEmployee, Login, Logout}

