import React, { Component } from 'react';
import cns from 'classnames';
import { Card, Select, Pagination } from '../../design';
import { Employee, EmployeeDetails } from '../employee';
import { IEmployee } from '../../types';
import {i18n} from "../../utils";

export interface Props {
  listTitle?: string
  data?: EmployeeDetails
  onPageChange?: Function
}

interface stateI {
  sortOrder: string,
  employee?: Array<IEmployee>
}

const sortOption = [
  { uuid: '', label: 'None' },
  { uuid: 'SALARY_-1', label: i18n('SALARY - Ascending') },
  { uuid: 'SALARY_1', label: i18n('SALARY - Descending') }
];
const defaultData = { totalEmployeeCount: 0, totalPageCount: 1, employee: [] };

export class List extends Component<Props> {
  _isMounted = false;
  state: stateI = {
    sortOrder: sortOption[0].uuid,
    employee: (this.props.data || defaultData).employee
  };

  static getDerivedStateFromProps(props: Props, state: stateI) {
    const { data } = props,
      { sortOrder } = state;
    let employee: Array<IEmployee> = data && data.employee ? JSON.parse(JSON.stringify(data.employee)) : [];
    sortOrder && employee.sort((a: IEmployee, b: IEmployee) => {
      const { SALARY: an } = a;
      const { SALARY: bn } = b;
      return sortOrder === 'SALARY_1'
        ? (an - bn)
        : (bn - an);
    });
    return { sortOrder, employee };
  }

  render() {
    const { listTitle, data = defaultData } = this.props;
    const { employee = [] } = this.state;
    const { totalEmployeeCount, totalPageCount } = data;
    return (
      <div className="list-group">
        <div className={cns('header', 'list-header')}>
          <Card title={listTitle}
                body={`Showing ${Math.floor(totalEmployeeCount / totalPageCount)} of ${totalEmployeeCount} result`}
                isBorderLess={true}/>
          <Card title={i18n("Sort by")}
                body={<Select name="sort_by" onChange={this.sortBy} options={sortOption}/>}
                isBorderLess={true}/>
        </div>
        <div className="list-group-items">
          {
            employee.length
              ? employee.map((details: IEmployee, ind: string | number | undefined) => <Employee key={ind} details={details}/>)
              : <div className="no-result">{i18n('No data found')}</div>
          }
          <Pagination totalPage={totalPageCount} onPageChange={this.onPageChange} />
        </div>
      </div>
    );
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
  sortBy = (name: string, val: {uuid: string}) => {
    this._isMounted && this.setState({ sortOrder: val.uuid });
  };

  onPageChange = (newPage: number) => {
    const {onPageChange} = this.props;
    onPageChange && onPageChange(newPage);
  };
}
