import React, { Fragment, PureComponent } from 'react';
import cns from "classnames";
import {Card, TextareaInput, Button} from '../../../design';
import { newReview } from '../../../types';
import {i18n} from "../../../utils";
export interface Props {
    className?:string
    onReviewSubmit: Function,

}
export class AddReview extends PureComponent<Props> {
    state:newReview={
        STAR: 0,
        REVIEW_L1: '',
        REVIEW_L2: '',
        REVIEW_L3: '',
    };
  render() {
      const {REVIEW_L1, REVIEW_L2, REVIEW_L3, STAR} = this.state;
      const starDiv = [1,2,3,4,5].map((i,ind)=>
          (ind< STAR ? <b className="star start-b clickable" key={ind} onClick={()=>this.onChange('STAR', i)}>★</b> : <span className="star clickable" key={ind} onClick={()=>this.onChange('STAR', i)}>☆</span>));
      return (
          <div className={cns("review-tile", this.props.className)}>
              <Card title={ <Fragment>{starDiv}</Fragment>} isBorderLess={true}/>
              <Card
                  title={ <TextareaInput name="REVIEW_L1" label={<span>{i18n("REVIEW_L1")}<sup>*</sup></span>} value={REVIEW_L1} onChange={this.onChange}/> }
                  body={<TextareaInput name="REVIEW_L2" label={<span>{i18n("REVIEW_L2")}<sup>*</sup></span>} value={REVIEW_L2} onChange={this.onChange}/>}
                  footer={<TextareaInput name="REVIEW_L3" label={<span>{i18n("REVIEW_L3")}<sup>*</sup></span>} value={REVIEW_L3} onChange={this.onChange}/>}
                  isBorderLess={true}/>
              <Card title={
                  <Button onClick={this.onReviewSubmit} disabled={!REVIEW_L1 || !REVIEW_L2 || !REVIEW_L3}>
                      {i18n('Add Review')}
                  </Button>
              } isBorderLess={true}/>
          </div>
      );
  }
  onChange = (name:string, value:string | number) => {
      this.setState({[name]: value});
  };
    onReviewSubmit = () => {
      console.log('this.state: ', this.state);
      this.props.onReviewSubmit(this.state);
  }
}

