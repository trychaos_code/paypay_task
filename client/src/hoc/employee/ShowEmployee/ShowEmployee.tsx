import React, {Fragment, PureComponent} from 'react';
import { IEmployee, newReview } from '../../../types';
import {compare, getMeta, i18n, Request} from '../../../utils';
import {Button, Card, Chip, Img, RequestLoader} from '../../../design';
import {Review} from "./Review";
import {timeDiff, getUserDet} from "../../../utils";
import {AddReview} from "./AddReview";
export interface Props {
  details: IEmployee
  onClose: () => void
}
const getUuid = () => Math.floor(Math.random() * 1000*1000);
export class ShowEmployee extends PureComponent<Props> {
  _isMounted = false;
  userDet = getUserDet();
  state = {
    addReview: false,
    reviewIdentifier: getUuid()
  };
  render() {
    const { details } = this.props;
    const { addReview, reviewIdentifier } = this.state;
    const {ID, PICTURE_URL, HIRED_ON, NAME, ABOUT, ...chipItems} = details;
    return (
      <div className="employee-details">
        <div className="employee-details-body">
          <Button className="bi-clear" onClick={this.onClose}>
            Close
          </Button>
          <div className="image-holder">
            <Img src={PICTURE_URL} alt={NAME}/>
          </div>
          <div className="pane">
            <div className="l-pane">
              <Card
                title={NAME}
                body={
                  <Fragment>
                    {
                      Object.entries(chipItems).map(([title, body], index) =>(<Chip key={index} title={i18n(title)} body={i18n(getMeta(title, body, true))}/>))
                    }
                    <Chip icon="/images/cal.png" body={timeDiff(HIRED_ON)} />
                    <div className="action-pane">
                      <Button disabled={ID===this.userDet.ID} className="pull-right" onClick={()=>this.setState({addReview: !addReview})}>{addReview ? i18n("Watch reviews"): i18n("Let's Review")}</Button>
                    </div>
                  </Fragment>
                }
                footer={
                  <div className="about-section">
                    {ABOUT}
                  </div>
                }
                isBorderLess={true}
              />
            </div>
            <div className="r-pane">
              {
                addReview
                    ? <AddReview className="add-review" onReviewSubmit={this.onReviewSubmit}/>
                    : <RequestLoader url={`/api/reviews?reviewIdentifier=${reviewIdentifier}&emp_id=${ID}`} loadingText={i18n("Loading reviews..")}>
                  <Review />
                </RequestLoader>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }

  onClose = () => {
    this.props.onClose();
  };
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
  onReviewSubmit = (review: newReview) => {
    const {details} = this.props;
    (async () => {
      const res = await new Request('/api/add-review', 'POST', {...review, GIVEN_BY: this.userDet.ID,GIVEN_TO: details.ID});
      if(res) {
        this._isMounted && this.setState({reviewIdentifier: getUuid(), addReview: false})
      }
    })();
  }
}

