import React, { Fragment, Component } from 'react';
import { Button, Select } from '../../design';

import {Ifilters, selectedFilters} from "../../types";
import {getMeta, i18n} from "../../utils";

export interface Props {
  onFilterChange: Function,
  filters: Ifilters
}
export class NavFilter extends Component<Props> {
  _isMounted = false;
  state:any = {
    filters: {...selectedFilters}
  };
  render() {
    const {filters} = this.props;
    const {filters:stateFilters} = this.state;
    return <div className="nav">
      <div className="nav-group">
        {
          Object.entries(filters).map(([label, options], ind)=> (
            <div className="nav-item" key={ind}>
              <Select selectedOption={stateFilters[label]} options={options} name={label} onChange={(name:string, value:string) => this.onFilterChange(ind, label, value)} formatter={(v: string)=>({label: i18n(getMeta(label, v, false)), uuid: v})} label={i18n(label)} />
            </div>
          ))
        }
        <div className="nav-item">
          <Button className="pull-right btn-info" onClick={this.onFilterReset}> {i18n('Reset Filter')} </Button>
          <Button className="pull-right" onClick={this.onFilterSubmit}> {i18n("Filter")} </Button>
        </div>
      </div>
    </div>;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
  onFilterChange = (ind: number, name: string, value: string)=> {
    const {filters} = this.state;
    // @ts-ignore
    filters[name] = value;
    this._isMounted && this.setState(filters);
  };
  onFilterReset =  ()=> {
    this._isMounted && this.setState({filters: {...selectedFilters}}, ()=>this.onFilterSubmit());
  };
  onFilterSubmit =  ()=> {
    const {filters} = this.state;
    console.log({filters});
    this.props.onFilterChange(filters);
  };
}
