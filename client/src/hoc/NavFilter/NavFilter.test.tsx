// ETA: 2 hours
import { cleanup, findAllByText, fireEvent, render, waitForElement } from '@testing-library/react';
import { NavFilter, Props } from './NavFilter';
import React from 'react';
import { IFiltersKeys} from '../../types';

describe('<NavFilter />', () => {
  function renderNavFilter(props: Partial<Props> = {}) {
    const defaultProps = {  filters: {}, onFilterChange: () => {}};
    const renderedElm = render(<NavFilter {...defaultProps} {...props} />);

    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("renders default", () => {
    renderNavFilter();
  });
});
