import { cleanup, findAllByText, queryAllByAttribute, render, waitForElement } from '@testing-library/react';
import { Header, Props } from './Header';
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Dashboard } from '../../views/Dashboard';
import { NotFound, AddEmployee } from '../../views';

describe('<Header />', () => {
  function renderHeader(props: Partial<Props> = {}) {
    const defaultProps = {  };
    const renderedElm =  render(<BrowserRouter>
      <Header {...defaultProps} {...props} />
      <Switch>
        <Route exact path="/" component={Dashboard} />
        <Route path="/add-employee" component={AddEmployee} />
        <Route path="*" component={NotFound} />
      </Switch>
    </BrowserRouter>);

    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();

    return renderedElm;
  }
  it("renders default", () => {
    renderHeader();
  });
  it('should render <Logo />', async () => {
    await (await waitForElement(() => queryAllByAttribute('src', document.body, ''))).forEach(textNode => {
      expect(textNode).toBeVisible();
    });
  });
  it('should render "My Orders" <Link to="/favorites" />', async () => {
    await (await waitForElement(() => queryAllByAttribute('href', document.body, ''))).forEach(textNode => {
      expect(textNode).toBeVisible();
    })
  });
});
