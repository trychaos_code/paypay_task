import React, { Fragment, PureComponent } from 'react';

export interface Props {
}
export class Footer extends PureComponent<Props> {
  render() {
    return <div className="footer">
      © Pay-Baymax 2018
    </div>;
  }
}
