// ETA: 30 minutes
import { findAllByText, fireEvent, render, waitForElement } from '@testing-library/react';
import React from 'react';

import {Footer, Props} from "./Footer"

describe('<Footer />', () => {
  function renderFooter(props: Partial<Props> = {}) {
    const defaultProps = {  };
    const renderedElm = render(<Footer {...defaultProps} {...props} />);

    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("renders default", () => {
    renderFooter();
  });
  const findItemByText =  (text: string) => {
    return async () => {
      const { findByText } = renderFooter();
      const navItem = await findByText(text);
      fireEvent.click(navItem);
      const container = document.body;
      const textNodes = await waitForElement(() => findAllByText(container, text));
      (await textNodes).forEach(textNode => {
        expect(textNode).toBeVisible();
      })
    }
  };
  it('should render "©Pay-Baymax 2019"', findItemByText('© Pay-Baymax 2018'));
});
