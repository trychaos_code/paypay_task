import { toDataURL } from './utills';

const iconList:object = {};
const imgArr = [
  "/images/employee.svg", "/images/logo.png"
];
imgArr.forEach(url =>{
  toDataURL(url, data => {
    // @ts-ignore
    iconList[url] = data;
  })
});
export default iconList;
