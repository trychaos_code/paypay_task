export const timeDiff = (time: string | Date | number) => {
    const secInterval = 1000,
        timeRef = time instanceof Date ? time : new Date(time),
        now = new Date(),
        diff = now.getTime() - timeRef.getTime();

    let totSec = Math.floor(diff / secInterval),
        totMinute = Math.floor(totSec / 60),
        totHour = Math.floor(totMinute / 60),
        totDays = Math.floor(totHour / 24),
        totMonths = Math.floor(totDays / 31),
        years = Math.floor(totMonths / 12);

    let sec = totSec - totMinute * 60,
        min = totMinute - totHour * 60,
        hours = totHour - totDays * 24,
        days = totDays - totMonths * 31,
        months = totMonths - years * 12;

    const strArr = [];
    years && strArr.push(`${years} years, `);
    months && strArr.push(`${months} months, `);
    days && strArr.push(`${days} days, `);
    hours && strArr.push(`${hours} hours and `);
    strArr.push(`${min} min`);
    //strArr.push(`${sec} sec`);

    return strArr.join('')
};
