import {config} from '../config/configs';
import {getToken, makeLogout} from "./auth"
export type Method = 'GET' | 'DELETE' | 'HEAD' | 'OPTIONS' | 'POST' | 'PUT' | 'PATCH';
export const validMethods:Array<Method> = ['GET', 'HEAD', 'DELETE', 'OPTIONS', 'POST', 'PUT', 'PATCH'];
export type RequestResponse = Response;
export type RequestError = Error;
export class Request {
  method: Method;
  url: string;
  param: object;
  constructor(url:string, method:Method = validMethods[0], param = {}) {
    const {origin, port} = config;
    if(!url.startsWith('http')){
      url = `${origin}:${port}${url.startsWith('/') ? '' : '/'}${url}`
    }
    this.method = validMethods.includes(method) ? method : validMethods[0];
    this.url = url;
    this.param = param;
  }
  then(callback= (res?: Response | null, err?: Error | null) => {}){
    (async ()=>{
      let token = getToken();
      const reqObj:RequestInit = {
        method:this.method,
        headers: {
          'Content-Type': 'application/json'
        },
        mode: "cors",
        cache: "force-cache",
        body: JSON.stringify(this.param || {})
      };
      if(['GET', 'HEAD'].includes(<string>reqObj.method)) {
        delete reqObj.body;
        if(token) {
          if(this.url.includes('?')){
            this.url += `&token=${token}`
          } else {
            this.url += `?token=${token}`
          }
        }
      } else if(token && typeof reqObj.body === "string") {
        reqObj.body = JSON.stringify({...JSON.parse(reqObj.body), token: encodeURIComponent(token)})
      }
      try {
        let response = await fetch(this.url, reqObj);
        response = await response.json();
        callback(response, null);
      } catch (error) {
        makeLogout();
      }
    })();
  }
}
