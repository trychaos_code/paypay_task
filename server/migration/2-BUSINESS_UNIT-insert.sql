INSERT INTO BUSINESS_UNIT (ID, NAME, LOCATION)
VALUES
(1, 'BU - 1', 'India'),
(2, 'BU - 2', 'ANDHRA PRADESH'),
(3, 'BU - 3', 'ASSAM'),
(4, 'BU - 4', 'ARUNACHAL PRADESH'),
(5, 'BU - 5', 'GUJRAT'),
(6, 'BU - 6', 'BIHAR'),
(7, 'BU - 7', 'HARYANA'),
(8, 'BU - 8', 'HIMACHAL PRADESH'),
(9, 'BU - 9', 'JAMMU & KASHMIR'),
(10, 'BU - 10', 'KARNATAKA'),
(11, 'BU - 11', 'USA');
