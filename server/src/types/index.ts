interface Entity {
  name: string
}

export interface IEmployee {
  ID: number,
  NAME: string,
  EMAIL: string,
  PASSWORD: string,
  JOB: string,
  MANAGER: number,
  HIRED_ON: string,
  SALARY: number,
  BU_NO: number,
  GRADE: number,
  TYPE: 'EXTERNAL' | 'INTERNAL' | 'CONTRACT' |  'CLIENT',
  AUTH_TYPE: 'ADMIN' | 'EMPLOYEE',
  PICTURE_URL: string,
  ABOUT: string
}
export interface IReview {
  ID: number,
  GIVEN_BY: number,
  GIVEN_TO: number,
  STAR: number,
  REVIEW_L1: string,
  REVIEW_L2: string,
  REVIEW_L3: string,
  REVIEWED_ON: string
}

export interface IBU {
  ID: number,
  NAME: string,
  LOCATION: string,
}

export interface IFilters {
  ID: Array<number>,
  NAME: Array<string>,
  JOB: Array<string>,
  MANAGER: Array<number>,
  HIRED_ON: Array<string>,
  SALARY: Array<number>,
  BU_NO: Array<number>,
  GRADE: Array<number>,
  TYPE: Array<'EXTERNAL' | 'INTERNAL' | 'CONTRACT' |  'CLIENT'>,
  AUTH_TYPE: Array<'ADMIN' | 'EMPLOYEE'>,
}
export const defaultFilters = {
  ID: {},
  NAME: {},
  JOB: {},
  MANAGER: {},
  HIRED_ON: {},
  SALARY: {},
  BU_NO:{},
  GRADE: {},
  TYPE: {},
  AUTH_TYPE: {},
};

export interface IEmployeeDet {
  employee: Array<IEmployee>,
  totalPageCount: number,
  totalEmployeeCount: number
}
export const defaultEmployeeDet:IEmployeeDet =  {
  employee: [],
  totalPageCount: 1,
  totalEmployeeCount: 0
};
export const fieldType =  {
  number :[
      'ID', 'MANAGER', 'SALARY', 'BU_NO', 'GRADE', 'GIVEN_BY', 'GIVEN_TO',  'STAR'
  ], string: [
      'NAME', 'JOB', 'HIRED_ON', 'TYPE', 'AUTH_TYPE', 'REVIEW_L1', 'REVIEW_L2', 'REVIEW_L3'
  ]
};

export interface IManufacturer {
  models: Entity []
  name: string
}
