import {addReviews, getEmployees, getMeta, getReviews, addEmployee, checkLogin} from '../controllers';
import {checkTokens} from "../middleware"

export const createRoutes = (app) => {
    app.get("/api/employee", checkTokens, getEmployees);
    app.get("/api/reviews", checkTokens, getReviews);

    app.post("/api/add-review", checkTokens, addReviews);
    app.post("/api/add-employee", checkTokens, addEmployee);

    app.get("/api/meta", getMeta);
    app.post("/api/login", checkLogin);
};
