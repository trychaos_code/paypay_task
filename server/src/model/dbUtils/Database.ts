import {Pool} from "mysql";

import {DBCred} from "../DBConfig/DBCred";
import {createDB, createDBPool} from "./DBPool";
import {executeQuery} from "./ExecQuery";

class Database {
    public pool: Pool;
    public name: string;

    constructor (name= DBCred.database) {
        this.name = name;
    }
    public async init () {
        await this.createDB();
        this.pool = <Pool> await this.createDBPool();
        return this;
    }

    public async createDB () {
        try {
            return await createDB(this.name);
        } catch (e) {
            console.log('createDB err', e);
        }
    }

    public async createDBPool () {
        try {
            return  await createDBPool(this.name)
        } catch (e) {
            console.log('createDB err', e);
        }
    };

    public async query ( sql ) {
        console.log('sql: ', sql);
        try {
            return await executeQuery(this.pool, sql);
        } catch (e) {
            console.log('query err', e)
        }
    };
}

let dbInstance = {};
export const getDataBase = async (name:string = DBCred.database) => {
    if (!dbInstance[name]) {
        const ins = new Database(name);
        return dbInstance[name] = await ins.init();
    }
    console.log(`current instances are: ${Object.keys(dbInstance)}`);
    return await new Promise<Database>(r => r(dbInstance[name]));
};
