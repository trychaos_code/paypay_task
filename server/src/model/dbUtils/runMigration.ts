import * as fs from "fs";
import {resolve} from "path";
import {getDataBase} from "./Database";
import {DBCred} from "../DBConfig/DBCred";

export const runMigration = async () => {
    const migrationPath = resolve(__dirname, '../../migration');
    const files: Array<string> = fs.readdirSync(migrationPath);
    files.sort();
    console.log('Migration files are : ', files);
    for (let file of files) {
        let content = fs.readFileSync(resolve(migrationPath, `./${file}`)).toString();
        content = content.split(/\n/g).join(' ');
        const db = await getDataBase(DBCred.database);
        await db.query(content);
    }

    return ;
};
