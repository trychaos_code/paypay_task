const { promisify } = require("util");
export const executeQuery = promisify((pool, query, args, callback) => {
    pool.getConnection((err, connection) => {
        if (err) {
            connection.release();
            callback(err)
        }
        connection.query(query, args,function(err,rows){
            connection.release();
            callback(err, {rows: rows});
        });
        connection.on('error', function(err) {
            callback(err);
        });
    });
});
