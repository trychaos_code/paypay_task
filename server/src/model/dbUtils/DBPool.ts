const { promisify } = require("util");

import {createPool, createConnection} from "mysql";
import {DBCred} from "../DBConfig/DBCred";
const {
    database,
    user,
    password,
    connectionLimit,
    port,
    host,
    multipleStatements
} = DBCred;

const createDB = promisify((db = database, callback) => {
    var con = createConnection({
        port,
        host,
        user,
        password
    });

    con.connect(function(err) {
        if (err) {
            callback(err);
        } else {
            console.log("DB Connected!");
            con.query(`CREATE DATABASE IF NOT EXISTS ${db}`, function (err, result) {
                if (err) {
                    callback(err);
                } else  {
                    console.log(`Database ${db} created`);
                    con.destroy();
                    callback(null, result)
                }
            });
        }
    });
});

const createDBPool = promisify((name, callback) => {
    try {
        const pool = createPool({
            database,
            user,
            password,
            connectionLimit,
            port,
            host,
            multipleStatements
        });
        pool.on('enqueue', function () {
            console.log('Waiting for available connection slot for DB: ', name);
        });
        console.log("DB Pool created of length: ", connectionLimit);
        callback(null, pool);
    } catch (err) {
        callback(err);
    }
});

export {createDB, createDBPool}
