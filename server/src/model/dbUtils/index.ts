import {getDataBase} from "./Database";
import {runMigration} from "./runMigration";
import {DBCred} from "../DBConfig/DBCred";
const {connectionLimit, database, port, password, user, host, multipleStatements} = DBCred;
export {
    getDataBase, runMigration, connectionLimit, database, port, password, user, host, multipleStatements
};

export default getDataBase;
