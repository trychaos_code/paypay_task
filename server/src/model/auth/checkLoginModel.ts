import {getDataBase, database} from "../dbUtils";

const cleanStr = (field:string) => (`'${field.split(/[^0-9a-zA-Z,-_\/\\\s\t]/).join(' ')}'`);

export async function checkLoginModel ({EMAIL, PASSWORD}): Promise<any> {
    const db = await getDataBase();
    return await db.query(`select * from ${database}.EMPLOYEE where EMAIL=${cleanStr(EMAIL)} and PASSWORD=${cleanStr(PASSWORD)}`);
}
