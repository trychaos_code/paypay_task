import {getReviewsModel} from "./getReviews";
import {addReviewsModel} from "./addReviews";

export {
    getReviewsModel, addReviewsModel
};

export default getReviewsModel;
