import {IReview} from "../../types";
import {getDataBase} from "../dbUtils";

const cleanStr = (field:string) => (`'${field.split(/[^0-9a-zA-Z,-_\/\\\s\t]/).join(' ')}'`);
const cleanInt = (field:number|string) => parseInt(field+'', 10) || 0;

const getQuery = ({GIVEN_BY, GIVEN_TO, STAR, REVIEW_L1 = '', REVIEW_L2 ='', REVIEW_L3 =''}) =>
    `INSERT INTO REVIEW (GIVEN_BY, GIVEN_TO, STAR, REVIEW_L1, REVIEW_L2, REVIEW_L3, REVIEWED_ON) VALUES `+
    `(${cleanInt(GIVEN_BY)}, ${cleanInt(GIVEN_TO)}, ${cleanInt(STAR)}, ${cleanStr(REVIEW_L1)}, ${cleanStr(REVIEW_L2)}, ${cleanStr(REVIEW_L3)}, curdate())`;
export async function addReviewsModel (data): Promise<any> {
    const db = await getDataBase();
    return await db.query(getQuery(data));
}
