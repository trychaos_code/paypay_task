import {IReview} from "../../types";
import {getDataBase, database} from "../dbUtils";

const getQuery = (emp_id: number, sortBy = 'REVIEWED_ON ASC') =>
    `select * from ${database}.REVIEW where GIVEN_BY = ${emp_id} or GIVEN_TO = ${emp_id} ORDER BY ${sortBy}`;
export async function getReviewsModel (emp_id): Promise<Array<IReview>> {
    const db = await getDataBase();
    return await db.query(getQuery(emp_id));
}
