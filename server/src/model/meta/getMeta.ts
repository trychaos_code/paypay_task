import {IEmployee, IBU} from "../../types";
import {getDataBase, database} from "../dbUtils";

export async function getEmployeesMetaModel (): Promise<Array<IEmployee>> {
    const db = await getDataBase();
    return await db.query(`select ID, NAME, PICTURE_URL, BU_NO, GRADE, TYPE, AUTH_TYPE from ${database}.EMPLOYEE ORDER BY NAME`);
}
export async function getBUMetaModel (): Promise<Array<IBU>> {
    const db = await getDataBase();
    return await db.query(`select * from ${database}.BUSINESS_UNIT ORDER BY NAME`);
}
