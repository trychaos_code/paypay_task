import {fieldType, IEmployee} from "../../types";
import {getDataBase, database} from "../dbUtils";

const getQuery = (filters: IEmployee, sortBy = 'name ASC') => {
    const keys = Object.keys(filters);
    let query = `select * from ${database}.EMPLOYEE ${keys.length ? 'where ' : ''}`;
    Object.entries(filters).forEach(([key, val], index, arr) => {
        const quotes = fieldType.number.includes(key) ? '' : "'";
        const chain = index === arr.length - 1 ? ' ' : " and ";
        query += `${key}=${quotes}${val}${quotes}${chain}`
    });
    query += `ORDER BY ${sortBy}`;
    return query;
};
export async function getEmployeesModel (filters, sortBy): Promise<Array<IEmployee>> {
    console.log({filters, sortBy});
    const db = await getDataBase();
    return await db.query(getQuery(filters, sortBy));
}
