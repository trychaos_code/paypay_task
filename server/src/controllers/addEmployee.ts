import { addEmployeeModel } from '../model';
import { Request, Response } from 'express';

export const addEmployee = async (req: Request, res: Response) => {
    const query = req.body || {};
    const {NAME,EMAIL,PASSWORD,PICTURE_URL,JOB,MANAGER,SALARY,BU_NO,GRADE,TYPE, AUTH_TYPE, ABOUT} = query;
    let insertResp:Array<any> = await addEmployeeModel({NAME,EMAIL,PASSWORD,PICTURE_URL,JOB,MANAGER,SALARY,BU_NO,GRADE,TYPE, AUTH_TYPE ,ABOUT});
    res.send({
        insertResp
    });
};
