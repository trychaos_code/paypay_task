import { addReviewsModel } from '../model';
import {IReview, IEmployeeDet, defaultFilters} from '../types';
import { Request, Response } from 'express';

export const addReviews = async (req: Request, res: Response) => {
    const query = req.body || {};
    const {GIVEN_BY, GIVEN_TO, STAR, REVIEW_L1, REVIEW_L2, REVIEW_L3} = query;
    let insertResp:Array<any> = await addReviewsModel({GIVEN_BY, GIVEN_TO, STAR, REVIEW_L1, REVIEW_L2, REVIEW_L3});
    res.send({
        insertResp
    });
};
