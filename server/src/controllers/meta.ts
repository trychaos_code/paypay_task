import { getEmployeesMetaModel, getBUMetaModel } from '../model';
import {IEmployee, IBU} from '../types';
import { Request, Response } from 'express';

export const getMeta = async (req: Request, res: Response) => {
  const convertMap = (arr: Array<IBU | IEmployee>) => arr.reduce((acc, {ID, ...rest}) => {
    acc[ID] = rest;
    return acc;
  },{});
  let employee:Array<IEmployee> = await getEmployeesMetaModel();
  let bu:Array<IBU> = await getBUMetaModel();
  res.send({
    employee: convertMap(employee), bu: convertMap(bu)
  });
};
