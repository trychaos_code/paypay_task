import { checkLoginModel } from '../model';
import { Request, Response } from 'express';
import {config} from "../config/configs"
import {sign} from "jsonwebtoken";

export const checkLogin = async (req: Request, res: Response) => {
    const query = req.body || {};
    const {EMAIL, PASSWORD:pass} = query;
    const usersFound = await checkLoginModel({EMAIL, PASSWORD:pass});
    const {PASSWORD, ...rest} = usersFound[0];
    console.log({rest});
    res.send({
        token: sign(rest, config.token_secret_key),
        userDet: rest
    });
};
