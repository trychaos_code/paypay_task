import {getEmployees} from "./employees";
import {getReviews} from "./reviews";
import {addReviews} from "./addReviews";
import {addEmployee} from "./addEmployee";
import {getMeta} from "./meta";
import {checkLogin} from "./checkLogin";

export {
    getReviews, getMeta, getEmployees, addReviews, addEmployee, checkLogin
};
