import { getReviewsModel } from '../model';
import {IReview, IEmployeeDet, defaultFilters} from '../types';
import { Request, Response } from 'express';

export const getReviews = async (req: Request, res: Response) => {
    const query = req.query || {};
    const {emp_id} = query;
    let filteredReviews:Array<IReview> = await getReviewsModel(emp_id);
    res.send({
        filteredReviews
    });
};
