import { getEmployeesModel } from '../model';
import {IEmployee, defaultFilters} from '../types';
import { Request, Response } from 'express';

const ITEMS_PER_PAGE = 10;

function paginate(collection: Array<IEmployee>, pageNo = 1) {
  const start = (pageNo - 1) * ITEMS_PER_PAGE;
  const end = start + ITEMS_PER_PAGE;
  return collection.slice(start, end);
}

export const getEmployees = async (req: Request, res: Response) => {
  const query = req.query || {};
  const {page = 1, sort, ...filters} = query;
  let filteredEmployees:Array<IEmployee> = await getEmployeesModel(filters, sort);
  const employee = paginate(filteredEmployees, Number(page));
  // @ts-ignore
  const {HIRED_ON, ID, SALARY, ...empFilters} = Object.entries(filteredEmployees.reduce((a,b)=>{
    Object.entries(b).forEach(([k,v])=>{
      a[k] && (a[k][v] = true);
    });
    return a;
  }, defaultFilters)).reduce((a, [k,v]) =>{
    a[k]= Object.keys(v);
    return a;
  }, {});
  res.send({
    employee,
    filters: empFilters,
    totalPageCount: Math.ceil(filteredEmployees.length / ITEMS_PER_PAGE),
    totalEmployeeCount: filteredEmployees.length
  });
};
